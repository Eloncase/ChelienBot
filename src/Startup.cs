﻿using ChelienBot.Core;
using ChipBot.Core;
using System.Reflection;
using System.Threading.Tasks;

namespace ChelienBot
{
    public class Startup
    {
        public static async Task<int> Main()
        {
            var startup = new Init();
            var exitCode = await startup.RunAsync(new() { Assembly.GetExecutingAssembly() }, new ChelienDbContextFactory());
            return exitCode;
        }
    }
}
