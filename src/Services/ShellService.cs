﻿using ChipBot.Core;
using ChipBot.Core.Services;
using System;
using System.Diagnostics;

namespace ChelienBot.Services
{
    public class ShellService : ChipService
    {
        public ShellService(LoggingService log) : base(log) { }

        public string Execute(string cmd, string args, int timeout = 30 * 1000)
        {
            try
            {
                var process = new Process()
                {
                    StartInfo = new ProcessStartInfo
                    {
                        FileName = cmd,
                        Arguments = args,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        UseShellExecute = false,
                        CreateNoWindow = true
                    }
                };

                process.Start();
                var result = process.StandardOutput.ReadToEnd();
                var error = process.StandardError.ReadToEnd();
                if (timeout > 0)
                {
                    process.WaitForExit(timeout);
                }
                else
                {
                    process.WaitForExit();
                }

                if (process.ExitCode != 0)
                {
                    throw new Exception(error);
                }

                return result;
            }
            catch (Exception ex)
            {
                throw new Exception($"Error running '{cmd}' with args '{args}': {ex.Message}", ex);
            }
        }
    }
}
