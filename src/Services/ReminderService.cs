﻿using ChelienBot.Core;
using ChipBot.Core;
using ChipBot.Core.Core.Database;
using ChipBot.Core.Services;
using Discord;
using Discord.WebSocket;
using System;
using System.Threading.Tasks;

namespace ChelienBot.Services
{
    public class ReminderService : ChipService
    {
        private readonly ClockService _clock;
        private readonly DiscordSocketClient _discord;
        private readonly IRepository<Reminder, DateTime> _repository;

        public ReminderService(LoggingService log, ClockService clock, DiscordSocketClient discord, IRepository<Reminder, DateTime> repository) : base(log)
        {
            _clock = clock;
            _discord = discord;
            _repository = repository;

            _clock.MinuteTick += CheckReminders;
        }

        public void SetReminder(DateTime date, ulong userId, string message)
        {
            Log(LogSeverity.Info, "Setting new reminder to " + date);
            _repository.Add(new Reminder() { UserId = userId, Text = message, DateTime = date });
        }

        private async Task CheckReminders()
        {
            Log(LogSeverity.Info, "Checking reminders");
            var reminders = _repository.GetAll();

            foreach(var reminder in reminders) {
                if (reminder.DateTime <= DateTime.UtcNow)
                {
                    Log(LogSeverity.Info, "Notifying " + reminder.UserId);
                    var channel = await _discord.GetUser(reminder.UserId).CreateDMChannelAsync();
                    await channel.SendMessageAsync(reminder.Text);
                    _repository.Remove(reminder.DateTime);
                }
            }
        }
    }
}
