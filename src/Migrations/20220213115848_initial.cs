﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace ChelienBot.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CommandExecutionModel",
                columns: table => new
                {
                    Name = table.Column<string>(type: "text", nullable: false),
                    DateTime = table.Column<DateTime>(type: "timestamp with time zone", nullable: false),
                    Parameters = table.Column<string>(type: "text", nullable: true),
                    FailedReason = table.Column<string>(type: "text", nullable: true),
                    ExecutorId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    ExecutorFriendlyName = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommandExecutionModel", x => new { x.Name, x.DateTime });
                });

            migrationBuilder.CreateTable(
                name: "CommandModel",
                columns: table => new
                {
                    Name = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CommandModel", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "DbConfigEntity",
                columns: table => new
                {
                    Name = table.Column<string>(type: "text", nullable: false),
                    Configuration = table.Column<string>(type: "jsonb", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbConfigEntity", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "DbStringEntity",
                columns: table => new
                {
                    Key = table.Column<string>(type: "text", nullable: false),
                    Value = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DbStringEntity", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "IssueModel",
                columns: table => new
                {
                    Name = table.Column<string>(type: "text", nullable: false),
                    Type = table.Column<int>(type: "integer", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Created = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IssueModel", x => x.Name);
                });

            migrationBuilder.CreateTable(
                name: "NotifierModel",
                columns: table => new
                {
                    ServiceName = table.Column<string>(type: "text", nullable: false),
                    Notified = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NotifierModel", x => x.ServiceName);
                });

            migrationBuilder.CreateTable(
                name: "Reminder",
                columns: table => new
                {
                    DateTime = table.Column<DateTimeOffset>(type: "timestamp with time zone", nullable: false),
                    UserId = table.Column<decimal>(type: "numeric(20,0)", nullable: false),
                    Text = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reminder", x => x.DateTime);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CommandExecutionModel_FailedReason",
                table: "CommandExecutionModel",
                column: "FailedReason");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CommandExecutionModel");

            migrationBuilder.DropTable(
                name: "CommandModel");

            migrationBuilder.DropTable(
                name: "DbConfigEntity");

            migrationBuilder.DropTable(
                name: "DbStringEntity");

            migrationBuilder.DropTable(
                name: "IssueModel");

            migrationBuilder.DropTable(
                name: "NotifierModel");

            migrationBuilder.DropTable(
                name: "Reminder");
        }
    }
}
