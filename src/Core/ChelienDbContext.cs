﻿using ChipBot.Core;
using ChipBot.Core.Core.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ChelienBot.Core
{
    public class ChelienDbContext : CoreDbContext
    {
        public ChelienDbContext(DbContextOptions options, IEnumerable<Type> entities) : base(options, entities) { }
    }

    public class ChelienDbContextFactory : CoreDbContextFactory<ChelienDbContext>, IDbContextFactory<DbContext>
    {
        public ChelienDbContextFactory() : base(new[] { Assembly.GetExecutingAssembly() })
        {
        }

        DbContext IDbContextFactory<DbContext>.CreateDbContext()
        {
            return CreateDbContext();
        }
    }
}
