﻿using ChelienBot.Services;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Module;
using Discord.Interactions;
using System;
using System.Threading.Tasks;

namespace ChelienBot.Modules
{
    [ChipCommandPrecondition(typeof(RequireChannel))]
    public class ReminderModule : ChipInteractionModuleBase
    {
        public ReminderService Service { get; set; }

        [SlashCommand("remind", "Reminds you to do something")]
        public async Task Remind([Summary(description: "Date and time when you want to get notified")] DateTime date, [Summary(description: "What you want to get notified about")] string message)
        {
            if (DateTime.UtcNow < date.ToUniversalTime())
            {
                Service.SetReminder(date.ToUniversalTime(), Context.User.Id, message);
                await ReplySuccessAsync();
            }
            else
            {
                await ReplyFailAsync();
            }
        }

        [SlashCommand("remind-in", "Reminds you to do something")]
        public async Task Remind([Summary(description: "Amount of minutes, hours or days")] TimeSpan timeSpan, [Summary(description: "What you want to get notified about")] string message)
        {
            await Remind(DateTime.UtcNow.Add(timeSpan), message);
        }

    }
}
