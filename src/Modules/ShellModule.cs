﻿using ChelienBot.Services;
using ChipBot.Core.Attributes;
using ChipBot.Core.Core.Preconditions;
using ChipBot.Core.Module;
using Discord.Interactions;
using System;
using System.Threading.Tasks;

namespace ChelienBot.Modules
{
    [RegisterToDevGuildOnly]
    [ChipCommandPrecondition(typeof(RequireChannel))]
    [ChipCommandPrecondition(typeof(RequireOwners))]
    public class ShellModule : ChipInteractionModuleBase
    {
        public ShellService Service { get; set; }

        [SlashCommand("execute", "Executes command")]
        public async Task InfoAsync([Summary(description: "Command to execute")]string cmd, [Summary(description: "Arguments to pass")]string args = null)
        {
            try
            {
                await RespondAsync($"Standard Output: ```{Service.Execute(cmd, args)}```");
            }
            catch (Exception ex)
            {
                await RespondAsync($"Command failed with exception: {ex.Message}");
            }
        }
    }
}
