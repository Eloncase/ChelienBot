﻿using ChipBot.Core;

namespace ChelienBot
{
    public class Bot : IBot
    {
        public long CreatedTS => 1552730669000;

        public string Description => "is a private-use Discord bot created to keep track of ChipBot's status.";

        public string Thumbnail => null;
    }
}
