﻿using ChipBot.Core.Core.Database;
using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;

namespace ChelienBot.Core
{
    public class Reminder : IDbEntity
    {
        [Required]
        public ulong UserId { get; set; }

        [Required]
        public string Text { get; set; }

        [Key]
        public DateTime DateTime { get; set; }
    }
}
