## v1.2.4.2 - 10.06.2020
### Fixed
* last page of steam store notification wasn't sent

## v1.2.4.1 - 09.06.2020
### Changed
* steam store notifications are now paged

## v1.2.4.0 - 13.05.2020
### Added
* updated core version
* updated services to support INotifySupport