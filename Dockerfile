FROM mcr.microsoft.com/dotnet/core/runtime:3.1

# Set up project dir
RUN mkdir /usr/local/chelienbot
COPY ./src/bin/Debug/netcoreapp3.1/publish/ /usr/local/chelienbot/publish

WORKDIR /usr/local/chelienbot

ENTRYPOINT ["dotnet", "publish/ChelienBot.dll"]
